# Full Stack Coding Challenge #

Welcome to the Edulink coding challenge.  

### Purpose ###

The purpose of this challenge is to gauge your coding ability and gain insight into how you would go about designing a solution. This challenge assumes you have experience in Angular, C#, and databases.


### The Challenge  ###

Develop a web application that will allow you to look up information for a publicly traded asset on the stock market and display details. It should display at a mimimum:

* the current stock price 
* 52 week trending information - Low, High, Change
* brief business summary of requested asset

The application should consist of the following technnical pieces:

1. Angular UI - Front end should make all calls through the backend application. Angular 11+ preferred.

2. C# web api backend - Back end should handle all webservice calls to retrieve data for the front end. 

Stock price data is available through Yahoo Finance (https://www.yahoofinanceapi.com). You can register for a free account and api key.


### Bonus points ###

* The Yahoo Finance free account limits the number of api requests per day to 100.  Cache response data to a database and show this data unless a refresh is requested from the UI.
* Provide visualizations for the stock data
* Unit tests

### How to submit your code challenge ###

This challenge should only take a couple hours. You are welcome to take more time if you like. When you are done, push your solution to your personal repo and send us a link. Create a readme file with the instructions to run the project and the tests, and add any comment that you think is relevant.